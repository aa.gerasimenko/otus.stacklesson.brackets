// See https://aka.ms/new-console-template for more information



public class Brackets
{
  public bool Check(string value)
  {
    int openBracket = 0;
    char openChar = '(';
    char closeChar = ')';

    if (String.IsNullOrEmpty(value))
      return true;

    foreach (var c in value)
    {
      if (c == openChar) openBracket++;
      if (c == closeChar)
      {
        openBracket--;
        if (openBracket < 0)
          return false;
      }
    }

    return openBracket == 0;
  }
}