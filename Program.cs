﻿// See https://aka.ms/new-console-template for more information
Console.WriteLine("Run Tests");

Test("without brackets", true, "without brackets");
Test("", true, "empty string");
Test("(())", true, "only brackets");
Test("( ( ()()    ))", true, "brackets inside brackets");
Test("(((((", false, "only openned");
Test("asd)))", false, "only closed");
Test("((asd)))", false, "more )");
Test("((asd)", false, "more (");
Test("asd)(()", false, "semantic error");

Console.WriteLine("Tests finihed");
// new brackets

void Test(string value, bool assert, string errorMessage)
{
  var brackets = new Brackets();
  if (!brackets.Check(value) == assert)
    Console.WriteLine("Failed for: {0}", errorMessage);
}
